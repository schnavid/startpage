import * as React from "react";

const Media = () => <div className="shortcut">
    <span className="title" id="media">media</span>
    <a href="https://youtube.com">youtube</a>
    <a href="https://netflix.com">netflix</a>
    <a href="https://open.spotify.com">spotify</a>
    <a href="https://disneyplus.com">disney+</a>
    <a href="https://crunchyroll.com">crunchyroll</a>
</div>;

const Coding = () => <div className="shortcut">
    <span className="title" id="coding">coding</span>
    <a href="https://gitlab.com">gitlab</a>
    <a href="https://github.com">github</a>
    <a href="https://medium.com">medium</a>
</div>;

const Social = () => <div className="shortcut">
    <span className="title" id="social">social</span>
    <a href="https://reddit.com">reddit</a>
    <a href="https://twitter.com">twitter</a>
    <a href="https://riot.im/app">riot</a>
</div>;

const Uni = () => <div className="shortcut">
    <span className="title" id="uni">uni</span>
    <a href="https://lsf.hhu.de">lsf</a>
    <a href="https://ilias.hhu.de">ilias</a>
    <a href="https://auas.math.uni-duesseldorf.de">auas (math)</a>
    <a href="https://auas.cs.uni-duesseldorf.de">auas (cs)</a>
    <a href="https://roundcube.hhu.de">roundcube</a>
</div>;

export default class Shortcuts extends React.Component {
    render() {
        return <div id="shortcuts">
            <Media/>
            <Coding/>
            <Social/>
            <Uni/>
        </div>;
    }
}