import * as React from "react";

export default class Clock extends React.Component<{}, { date: Date }, {}> {
    timerID: number;

    constructor(props: {}) {
        super(props);
        this.state = { date: new Date() };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000,
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        function pad(value: number) {
            let val = String(value);
            while (val.length < 2) {
                val = '0' + val;
            }
            return val;
        }

        function format(date: Date) {
            let hours = pad(date.getHours());
            let minutes = pad(date.getMinutes());

            return hours + ":" + minutes;
        }

        return <h1 className="clock">
            {format(this.state.date)}
        </h1>;
    }
}