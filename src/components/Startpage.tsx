import * as React from "react";

import Clock from "./Clock";
import Shortcuts from "./Shortcuts";

export default class Startpage extends React.Component {
    render() {
        return <div id="startpage">
            <Clock/>
            <Shortcuts/>
        </div>
    }
}