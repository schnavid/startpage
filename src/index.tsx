import * as React from "react";
import * as ReactDOM from "react-dom";

import Startpage from "./components/Startpage";

import "./sass/style.scss";

ReactDOM.render(
    <Startpage/>,
    document.getElementById("app")
);